﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public float ballSpeed;
    private Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        //Set random start direction

        if(Random.Range(0,100) > 50)
        {
            rb.velocity = Vector2.right * ballSpeed;
        }
        else
        {
            rb.velocity = -Vector2.right * ballSpeed;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Goal"))
        {
            Destroy(gameObject);
        }
        
    }
}

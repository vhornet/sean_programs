﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour
{
    public bool player1Goal, player2Goal;
    public ScoreManager sm;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Ball"))
        {
            if (player2Goal)
            {
                sm.player1Score++;
                sm.p1ScoreText.text = sm.player1Score.ToString();
            }

            if (player1Goal)
            {
                sm.player2Score++;
                sm.p2ScoreText.text = sm.player2Score.ToString();
            }

            sm.StartCoroutine("BallSpawn");
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleController : MonoBehaviour
{
    public GameObject paddle1, paddle2;
    public float paddleSpeed;

    public float yMin, yMax;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        P1Move();
        P2Move();
    }

    //Player1 Controls
    private void P1Move()
    {
        if(Input.GetAxisRaw("Horizontal") == 1 && paddle1.transform.position.y < yMax)
        {
            paddle1.transform.position += Vector3.up * paddleSpeed * Time.deltaTime;
        }
        if(Input.GetAxisRaw("Horizontal") == -1 && paddle1.transform.position.y > yMin)
        {
            paddle1.transform.position += -Vector3.up * paddleSpeed * Time.deltaTime;
        }
    }

    //Player2 Controls
    private void P2Move()
    {
        if (Input.GetAxisRaw("Vertical") == 1 && paddle2.transform.position.y < yMax)
        {
            paddle2.transform.position += Vector3.up * paddleSpeed * Time.deltaTime;
        }
        if (Input.GetAxisRaw("Vertical") == -1 && paddle2.transform.position.y > yMin)
        {
            paddle2.transform.position += -Vector3.up * paddleSpeed * Time.deltaTime;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public GameObject ballToSpawn;
    public int player1Score, player2Score;
    public float ballDelay;
    public Text p1ScoreText, p2ScoreText;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("BallSpawn");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator BallSpawn()
    {
        yield return new WaitForSeconds(ballDelay);

        Instantiate(ballToSpawn, transform.position, Quaternion.identity);
    }
}
